export const getUrlParameter = (url, paramname) => {
    const param = paramname.replace(/[[]/, '\\[').replace(/[\]]/, '\\]');
    const regex = new RegExp(`[\\?&]${param}=([^&#]*)`);
    const results = regex.exec(url);
    return results === null
        ? ''
        : decodeURIComponent(results[1].replace(/\+/g, ' '));
};