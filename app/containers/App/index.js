/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from 'containers/Home/Loadable';
import Overview from 'containers/Overview/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
// import IuPage from 'containers/IuPage';
import styled from 'styled-components';
import GlobalStyle from '../../global-styles';


import { Layout } from 'antd';

const { Header } = Layout;

const AppLayout = styled(Layout)`
  background-color: #FFF;
`

const AppHeader = styled(Header)`
  color:#FFF;
`

export default function App() {
  return (
    <AppLayout>
      <AppHeader>
        Employee Directory v0.1
      </AppHeader>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/overview" component={Overview} />
        <Route component={NotFoundPage} />
      </Switch>
      <GlobalStyle />
    </AppLayout>
  );
}
