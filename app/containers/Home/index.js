/**
 *
 * Home
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectHome from './selectors';
import reducer from './reducer';
import saga from './saga';

import { Input, Breadcrumb } from 'antd';
const { Search } = Input;
import Content from '../../components/Content';
import { push } from 'connected-react-router';
import makeSelectOverview from 'containers/Overview/selectors';
import styled from 'styled-components';

const EmployeeItem = styled.a`
  margin-right:20px;
`

export function Home({
  overview,
  goToOverViewPage
}) {
  useInjectReducer({ key: 'home', reducer });
  useInjectSaga({ key: 'home', saga });

  const onSearch = (text) => {
    if (text)
      goToOverViewPage(text);
  }

  return <Content>
    <Breadcrumb style={{ margin: '16px 0' }}>
      <Breadcrumb.Item>Home</Breadcrumb.Item>
    </Breadcrumb>
    <Content style={{ minHeight: '200px', padding: 0, display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
      <Search
        placeholder="Employee name"
        allowClear
        enterButton="Search"
        size="large"
        style={{ maxWidth: '500px', width: '%70' }}
        onSearch={onSearch}
      />
      {overview.data && <div style={{ marginTop: '50px' }}>
        Fetched employees:<br />
        {Object.keys(overview.data).map(i => <EmployeeItem key={i} onClick={() => { onSearch(i) }}>{i}</EmployeeItem>)}
      </div>}
    </Content>

  </Content>;
}

Home.propTypes = {
  dispatch: PropTypes.func.isRequired,
  goToOverViewPage: PropTypes.func,
  overview: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  home: makeSelectHome(),
  overview: makeSelectOverview()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    goToOverViewPage: (name) => dispatch(push(`/overview?name=${name}`)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Home);
