import { QUERY, QUERY_URL, SUB_KEY } from "./constants";
import request from 'utils/request';
import { call, put, takeEvery, select } from 'redux-saga/effects';
import { querySuccess, queryFail, query } from './actions';


const getData = (state) => state.overview.data;

export function* querySaga(action) {
  try {
    const url = QUERY_URL;
    let data = yield select(getData);
    let name = action.name;
    if (data && data[name]) {
      yield put(querySuccess(name, data[name]));
    }
    else {
      const data = yield call(request, url + action.name, {
        method: 'GET'
      });
      let subs = data && data[1] && data[1][SUB_KEY];
      yield put(querySuccess(action.name, data));
      if (subs)
        for (let i of subs) {
          yield put(query(i));
        }
    }
  } catch (err) {
    yield put(queryFail(err));
  }
}


export default function* overviewSaga() {
  yield takeEvery(QUERY, querySaga);
}
