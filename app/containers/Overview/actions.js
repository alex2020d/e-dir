/*
 *
 * Overview actions
 *
 */

import { QUERY_SUCCESS, QUERY, QUERY_FAIL } from './constants';


export function query(name) {
  return {
    type: QUERY,
    name: name
  };
}


export function querySuccess(name, data) {
  return {
    type: QUERY_SUCCESS,
    name: name,
    data: data
  };
}



export function queryFail(error) {
  return {
    type: QUERY_FAIL,
    error: error
  };
}
