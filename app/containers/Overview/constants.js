/*
 *
 * Overview constants
 *
 */

export const QUERY = 'app/Overview/QUERY';
export const QUERY_FAIL = 'app/Overview/QUERY_FAIL';
export const QUERY_SUCCESS = 'app/Overview/QUERY_SUCCESS';

export const QUERY_URL = 'http://api.additivasia.io/api/v1/assignment/employees/'
export const SUB_KEY = 'direct-subordinates'