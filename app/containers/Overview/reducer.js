/*
 *
 * Overview reducer
 *
 */
import produce from 'immer';
import { QUERY, QUERY_SUCCESS, QUERY_FAIL } from './constants';

export const initialState = {};

/* eslint-disable default-case, no-param-reassign */
const overviewReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case QUERY:
        draft.loading = true;
        break;
      case QUERY_SUCCESS:
        if (!draft.data)
          draft.data = {}
        draft.data[action.name] = action.data;
        draft.loading = false;
        break;
      case QUERY_FAIL:
        draft.error = action.error;
        draft.loading = false;
        break;
    }
  });

export default overviewReducer;
