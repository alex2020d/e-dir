/**
 *
 * Overview
 *
 */

import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { getUrlParameter } from 'utils/helper';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectOverview, { makeSelectLocationSearch } from './selectors';
import reducer from './reducer';
import saga from './saga';
import Content from '../../components/Content';
import { Breadcrumb, Tree, Button } from 'antd';
import { query } from './actions';
import { useHistory } from "react-router-dom";

export function buildTreeFromMap(map = {}, name, parentKey = '0', index = 0, set = new Set()) {
  const key = `${parentKey}-${index}`;
  if (!map[name] || set.has(name))
    return [null, []];
  let keys = [key];
  let node = {
    title: `${name} (${map[name][0]})`,
    key: key,
    children: []
  }
  set.add(name);
  if (map[name][1]) {
    const subs = map[name][1]['direct-subordinates'];
    for (let i = 0; i < subs.length; i++) {
      let [child, childKeys] = buildTreeFromMap(map, subs[i], key, i, set);
      if (child) {
        node.children.push(child);
        keys = keys.concat(childKeys);
      }
    }
  }
  return [node, keys];
}

export function Overview({ overview, onQuery, searchParams }) {
  const name = getUrlParameter(searchParams, 'name')
  useInjectReducer({ key: 'overview', reducer });
  useInjectSaga({ key: 'overview', saga });
  const [treeData, setTreeData] = useState({});
  const history = useHistory();

  useEffect(() => {
    let [tree, keys] = buildTreeFromMap(overview.data, name);
    setTreeData({
      tree: tree,
      keys: keys
    });
  }, [overview])

  useEffect(() => {
    let name = getUrlParameter(searchParams, 'name');
    if (name)
      onQuery(name);
  }, [])

  const back = () => {
    history.goBack();
  }


  return <Content>
    <Breadcrumb style={{ margin: '16px 0' }}>
      <Breadcrumb.Item>Overview</Breadcrumb.Item>
    </Breadcrumb>
    <Content style={{ minHeight: '300px', padding: 0, display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
      {treeData.tree ? <Tree treeData={[treeData.tree]} expandedKeys={treeData.keys} /> : 'No data found'}
      <Button onClick={back} style={{ marginTop: '50px' }}>Back</Button>
    </Content>
  </Content >;
}

Overview.propTypes = {
  dispatch: PropTypes.func.isRequired,
  searchParams: PropTypes.string,
  overview: PropTypes.object,
  onLookUpIU: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  overview: makeSelectOverview(),
  searchParams: makeSelectLocationSearch(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onQuery: (name) =>
      dispatch(query(name)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Overview);
